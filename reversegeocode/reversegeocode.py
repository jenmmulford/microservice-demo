from flask import Flask, request, Response, stream_with_context
import requests
import logging


app = Flask(__name__)
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger("reversegeocode.py")


@app.route('/reversegeocode')
def proxy():
    r = fetch_reversegeocode()
    LOG.info("Got %s response", r.status_code)
    return Response(stream_with_context(r.iter_content()),
                    content_type=r.headers['content-type'])


def fetch_reversegeocode():
    payload = {'api_key': request.headers['X-API-Key'],
               'coordinate': request.args['coordinate']}
    return requests.get(
        'https://apis.location.studio/geo/v2/reversegeocode/json',
        params=payload)
